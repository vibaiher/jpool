package org.vibaiher.jpool;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PoolTest {
    @Test
    @DisplayName("as a singleton, produces always same instance")
    void producesSameInstance() {
        Pool one_instance = Pool.get();
        Pool another_instance = Pool.get();

        assertEquals(one_instance, another_instance);
        assertNotNull(one_instance);
    }
}
