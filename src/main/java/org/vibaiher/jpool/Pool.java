package org.vibaiher.jpool;

public class Pool {
    static Pool pool;

    public static Pool get() {
        if (pool == null) {
            pool = new Pool();
        }

        return pool;
    }

    private Pool() {
    }
}
